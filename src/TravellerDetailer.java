public class TravellerDetailer {
    String nameOfTraveller;
    float weightOfLuggage;


    public TravellerDetailer( String nameOfTraveller, float weightOfLuggage) {
        this.nameOfTraveller = nameOfTraveller;
        this.weightOfLuggage =weightOfLuggage ;

    }

    public String getNameOfTraveller() {
        return nameOfTraveller;
    }


    public float getWeightOfLuggage() {
        return weightOfLuggage;
    }


}
