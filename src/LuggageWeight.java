import java.util.Scanner;

public class LuggageWeight {

    public static void luggageNotAllowed() throws LuggageWeightException {
        throw new LuggageWeightException("not allowed");
    }

    public static void isLuggageAllowed(TravellerDetailer travelerInformation){
        try {
            if (travelerInformation.weightOfLuggage >15.1f) {
                luggageNotAllowed();
            }
            else{
                System.out.println("your luggage is allowed without any extra charge");
            }

        }
        catch(LuggageWeightException e){
            float extraCharge=(travelerInformation.weightOfLuggage -15)*500;
            System.out.println("You have to pay extra "+extraCharge+" as charge");
        }
        finally {
            System.out.println("You can go now");
        }
    }

    public static void main(String[] args){

        TravellerDetailer firstTraveller=new TravellerDetailer("Bhavya",15.1f) ;
        System.out.println(firstTraveller.nameOfTraveller);
        isLuggageAllowed(firstTraveller );
        TravellerDetailer secondTraveller=new TravellerDetailer("Shruti",18);
        System.out.println(secondTraveller.nameOfTraveller);
        isLuggageAllowed(secondTraveller );


    }
}

