public class LuggageWeightException extends Exception {
    public LuggageWeightException(String message) {
        super("Luggage weight exceed ");
    }
}
